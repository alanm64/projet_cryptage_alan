#include <stdio.h>
#include <stdlib.h>
#include "decryptage.h"
#include "lecturePerroquet.h"


void decrypter(void){
    int ret, ret2, i=0, rep;
    char fname[20];
    char fname2[20];
    char perro[20];
    FILE *fp=NULL;
    FILE *fp2=NULL;

    printf("\nveuillez saisir le nom du fichier crypte: ");
    fflush(stdin);
    scanf("%s", &fname);

    fp = fopen(fname, "r");
    if (fp == NULL)
    {
        printf("Erreur ouverture fichier source !!\n");
        return EXIT_FAILURE;
    }

    rep = perroquet(&perro);

    char choix;
    do{
        printf("\nveuillez saisir le nom du fichier decrypte: ");
        fflush(stdin);
        scanf("%s", &fname2);
        fp2 = fopen(fname2, "r");
        if (fp2 != NULL)
        {
            fclose(fp2);
            printf("\nLe fichier existe deja!");
            printf("\nVoulez-vous l'ecraser ? [o/N]: ");
            fflush(stdin);
            scanf("%c", &choix);
            if(choix=='o' || choix=='O'){
                fp2 = fopen(fname2, "w+");
                printf("Le fichier a ete ecrase!");
            }
            else{
                fp2=NULL;
            }
        }
        else{
            fclose(fp2);
            fp2 = fopen(fname2, "w+");
            printf("Le fichier a ete cree");
        }
    }while(fp2==NULL);


    char lettre;

    fp2 = fopen(fname2, "w+");

    while(!feof(fp)){
        fread(&lettre, sizeof(lettre), 1, fp);
        lettre+=perro[i];
        if(i<rep-1){
            i++;
        }
        else{
            i=0;
        }

        if (!feof(fp))
        {
            fwrite(&lettre, sizeof(lettre), 1, fp2);
        }


    }

    ret=fclose(fp);
    if (ret != 0)
    {
        printf("Erreur Close !!");
        return EXIT_FAILURE;
    }
    ret2 = fclose(fp2);
    if (ret2 != 0)
    {
        printf("Erreur Close !!");
        return EXIT_FAILURE;
    }
    printf("\n--Fin du decryptage--\n");

}

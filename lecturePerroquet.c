#include <stdio.h>
#include <stdlib.h>
#include "lecturePerroquet.h"

/*Cette fonction prend en entr�e une liste de char vide qui sera
compl�t�e par les �l�ments du fichier peroq.def
Elle retourne le nombre d'�l�ments r�cup�r�s dans le
fichier perroq.def*/

int perroquet(char *perro){
    FILE *fp = NULL;
    char *p;
    int i=0, rep;
    char let;

    fp=fopen("peroq.def", "r");
    if(fp==NULL){
        printf("erreur ouverture");
    }
    p=perro;
    while(!feof(fp)){
        fread(&let, sizeof(let), 1, fp);
        *p=let;
        p++;
        i++;
    }
    rep = fclose(fp);
    if(rep!=0){
        printf("erreur fermeture");
    }
    return (i-1);
}

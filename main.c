#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "cryptage.h"
#include "decryptage.h"


int main()
{
    int choix;
    do{
        printf("\n---------Menu-----------\n");
        printf("1. Crypter\n");
        printf("2. Decrypter\n");
        printf("3. Quitter\n");
        printf("-> ");
        fflush(stdin);
        scanf("%d", &choix);
        switch (choix){
        case 1:
            printf("\nVous avez choisi de crypter un fichier");
            crypter();
            break;
        case 2:
            printf("\nVous avez choisi de decrypter un fichier");
            decrypter();
            break;
        case 3:
            printf("\nvous avez choisi de quitter\n");
            break;
        default:
            printf("Mauvaise commande!\n");
            break;

        }
    }while(choix!=3);


    return 0;
}




